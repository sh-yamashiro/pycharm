#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@citation: 
"""

import os
import re
import codecs


cp932_dict = {
    '12168': '-', '50872': '^+', '50873': '^-', '50876': '_1',
    '50877': '_2', '50878': '_3', '50879': '_4', '50880': '_5',
    '50881': '_6', '50882': '_7', '50883': '_8', '50884': '_9',
    '50885': '_0', '50891': '^1', '50892': '^2', '50893': '^3',
    '50894': '^4', '50895': '^5', '50896': '^6', '50897': '^7',
    '50898': '^8', '50899': '^9', '50900': '^0', '50889': '_/',
    '50925': '_A', '50926': '_B', '50927': '_C', '50928': '_D',
    '50932': '_M', '50933': '_N', '61782': 'K', '62078': '*',
}

DIR = "C:\\Users\\shunsuke\\Downloads\\python_pdf\\"

csv_folder_path = DIR                               # 現在のフォルダのパスを取得
text_folder_path = DIR + 'text_folder' + '\\'       # pathの表記がmac仕様。windowsの場合は、'/'を'\'に修正する。

os.makedirs(text_folder_path, exist_ok=True)
text_file_name = os.listdir(text_folder_path)


# PDFをtextファイルに変換
def main(name):
    pattern = re.compile(r'\(cid:[0-9]{5}\)')
    pattern_page = re.compile(r'\(cid:12168\) [0-9]+\(cid:12168\)')
    f = open(text_folder_path + name, 'r')
    fo = open(csv_folder_path + name.replace('.PDF.txt', '.txt'), 'w')

    flg = 0
    line = f.readline()
    while line:
        # ヘッダー処理
        if flg == 0:
            if line.startswith('◎'):
                flg = 1
                line = f.readline()
                continue
        elif flg == 1:
            if '】' in line:
                flg = 2
            line = f.readline()
            continue

        # 空行, ページ番号は除く
        if line == '\n' or re.search(pattern_page, line):
            line = f.readline()
            continue

        # cp932のデコード
        ms = re.findall(pattern, line)
        for m in ms:
            cid = m[5:10]
            if cid in cp932_dict.keys():
                line = line.replace(m, cp932_dict[cid])

        if line.startswith('\f問'):
            line = line.replace('\f問', '問')
        fo.write(line)
        line = f.readline()
    f.close()
    fo.close()

if __name__ == '__main__':
    # main('102-5.PDF.txt')
    for name in text_file_name:
        print(name)
        main(name)

