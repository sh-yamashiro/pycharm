#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@citation: https://qiita.com/checkpoint/items/d9bcc63292d7f01c62d3
"""

import requests
import time
import urllib.parse as urlparse
import re

from bs4 import BeautifulSoup

EXTENSION = ".PDF"
DIR = "C:\\Users\\shunsuke\\Downloads\\python_pdf\\"


def main(url: str, regex: str='.PDF'):

    o = urlparse.urlparse(url)
    BASE_URL = o.scheme + '://' + o.netloc + '/'
    download_urls = []
    r = requests.get(url)
    soup = BeautifulSoup(r.content)
    links = soup.findAll('a')
    pattern = re.compile(regex)

    # URLの抽出
    for link in links:

        href = link.get('href')

        if href and re.search(pattern, href):
            dl_dir = 'seisakunitsuite/bunya/kenkou_iryou/iyakuhin/yakuzaishi-kokkashiken/'
            download_urls.append(dl_dir + href)

    # ファイルのダウンロード（ひとまず3件に制限）
    for download_url in download_urls:

        # 一秒スリープ
        time.sleep(1)

        file_name = download_url.split("/")[-1]

        if BASE_URL in download_url:
            r = requests.get(download_url)
        else:
            r = requests.get(BASE_URL + download_url)

        # ファイルの保存
        if r.status_code == 200:
            f = open(DIR + file_name, 'wb')
            f.write(r.content)
            f.close()

if __name__ == '__main__':
    urls = [
        "http://www.mhlw.go.jp/stf/seisakunitsuite/bunya/0000168886.html",
        "http://www.mhlw.go.jp/stf/seisakunitsuite/bunya/0000117691.html",
        "http://www.mhlw.go.jp/stf/seisakunitsuite/bunya/kenkou_iryou/iyakuhin/yakuzaishi-kokkashiken/kakomon-100.html",
        "http://www.mhlw.go.jp/stf/seisakunitsuite/bunya/kenkou_iryou/iyakuhin/yakuzaishi-kokkashiken/kakomon-99.html",
        "http://www.mhlw.go.jp/seisakunitsuite/bunya/kenkou_iryou/iyakuhin/yakuzaishi-kokkashiken/kakomon-98.html",
        "http://www.mhlw.go.jp/seisakunitsuite/bunya/kenkou_iryou/iyakuhin/yakuzaishi-kokkashiken/kakomon-97.html",
    ]
    for url in urls[-2:]:
        main(url)
