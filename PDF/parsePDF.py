#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@citation: http://irukanobox.blogspot.jp/2017/03/python3pdf.html
"""

import os
import re
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPageInterpreter
from io import BytesIO

DIR = "C:\\Users\\shunsuke\\Downloads\\python_pdf\\"

pdf_folder_path = DIR                           # 現在のフォルダのパスを取得
text_folder_path = DIR + 'text_folder' + '\\'        # pathの表記がmac仕様。windowsの場合は、'/'を'\'に修正する。

os.makedirs(text_folder_path, exist_ok=True)
pdf_file_name = os.listdir(pdf_folder_path)


# name がPDFファイル（末尾が.PDF）の場合はTRUE、それ以外はFALSEを返す。
# こちらの投稿を引用・一部変更しました　→　http://qiita.com/korkewriya/items/72de38fc506ab37b4f2d
def pdf_checker(name):
    pdf_regex = re.compile(r'.+\.PDF')
    if pdf_regex.search(str(name)):
        return True
    else:
        return False


# PDFをtextファイルに変換
def convert_pdf_to_txt(name, txtname, buf=True):
    rsrcmgr = PDFResourceManager()
    if buf:
        outfp = BytesIO()
    else:
        outfp = open(txtname, 'w')
    codec = 'utf-8'
    laparams = LAParams()
    laparams.detect_vertical = True
    device = TextConverter(rsrcmgr, outfp, codec=codec, laparams=laparams)

    fp = open(pdf_folder_path + name, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    for page in PDFPageInterpreter.get_pages(fp):
        interpreter.process_page(page)
    fp.close()
    device.close()
    if buf:
        text = outfp.getvalue()
        make_new_text_file = open(text_folder_path + name + '.txt', 'wb')
        make_new_text_file.write(text)
        make_new_text_file.close()
    outfp.close()


if __name__ == '__main__':
    for name in pdf_file_name:
        if pdf_checker(name):
            print(name)
            convert_pdf_to_txt(name, name + '.txt')     # pdf_checkerを使い、TRUE（末尾が.pdfの場合）は変換に進む）
        else:
            pass                                        # PDFファイルでない場合にはpass

