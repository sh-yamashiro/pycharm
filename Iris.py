# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# Iris データをロード
from sklearn import datasets
import seaborn as sns


def main_1():
    iris = datasets.load_iris().data

    # 散布図
    plt.figure(1)
    plt.scatter(iris[:, 1], iris[:, 2])
    # plt.show()

    # 箱ひげ図
    plt.figure(2)
    plt.boxplot(iris[:, 0])
    # plt.show()

    # ヒストグラム
    plt.figure(3)
    plt.hist(iris[:, 0])
    # plt.show()

    # 折れ線グラフ
    plt.figure(4)
    plt.plot(iris[:, 0])
    # plt.show()

    # 円グラフ
    plt.figure(5)
    crosstab = pd.crosstab(datasets.load_iris().target, columns="Target")
    dict = crosstab['Target'].to_dict()
    plt.pie(list(dict.values()), labels=list(dict.keys()))
    # plt.show()

    # 棒グラフ
    plt.figure(6)
    ind = np.arange(len(iris[:, 1]))
    plt.bar(ind, iris[:, 0])
    # plt.show()

    plt.show()


def main_2():
    sns.set_style('darkgrid')
    sns.set_palette('deep')
    # tips = sns.load_dataset("tips")
    # sns.regplot(x="total_bill", y="tip", data=tips)
    # sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips, markers=["o", "x"])

    iris = sns.load_dataset('iris')
    sns.jointplot('sepal_width', 'petal_width', data=iris)
    sns.pairplot(iris, hue="species", diag_kind='kde')

    plt.show()


if __name__ == '__main__':
    main_2()
