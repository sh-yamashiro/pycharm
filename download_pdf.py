#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@citation: https://qiita.com/checkpoint/items/d9bcc63292d7f01c62d3
"""

import requests
import time
import urllib.parse as urlparse
import os
import re
import tqdm

from bs4 import BeautifulSoup

DOWNLOAD_DIR = "C:\\Users\\VAIO_PC\\Downloads\\python_pdf\\"


def main(url: str, regex: str='.pdf', save_dir=DOWNLOAD_DIR):
    # 保存先フォルダを確認、なければ作成
    if not os._exists(save_dir):
        os.makedirs(save_dir, )

    # リンク元のページを取得
    o = urlparse.urlparse(url)
    BASE_URL = o.scheme + '://' + o.netloc + '/'

    # aタグ一覧を取得
    download_urls = []
    r = requests.get(url)
    soup = BeautifulSoup(r.content, "lxml")
    links = soup.findAll('a')

    # pattternマッチしたURLの抽出
    pattern = re.compile(regex)
    for link in links:
        href = link.get('href')
        if href and re.search(pattern, href):
            download_urls.append(href)

    # ファイルのダウンロード
    for download_url in tqdm.tqdm(download_urls):
        time.sleep(1)

        file_name = download_url.split("/")[-1]
        if BASE_URL in download_url:
            r = requests.get(download_url)
        else:
            r = requests.get(BASE_URL + download_url)

        # ファイルの保存
        if r.status_code == 200:
            f = open(save_dir + file_name, 'wb')
            f.write(r.content)
            f.close()

if __name__ == '__main__':
    urls = {
        '熱性けいれん診療(2015)': "https://www.childneuro.jp/modules/about/index.php?content_id=33",
        '小児急性脳症診療(2016)': "https://www.childneuro.jp/modules/about/index.php?content_id=34",
        '小児けいれん重積治療(2017)': "https://www.childneuro.jp/modules/about/index.php?content_id=36"
    }
    for key, url in urls.items():
        time.sleep(1)

        print(key)
        main(url, save_dir=DOWNLOAD_DIR+key+'\\')
