# coding: UTF-8

import random
import time

import mysql.connector

base_url = 'https://ja.wikipedia.org'

connector = mysql.connector.connect(
    host="localhost", user="root", passwd="root", db="wikipedia", charset="utf8mb4")
cursor = connector.cursor(buffered=True)

def getUrl(pageId):
    sql = f"""
    SELECT url FROM pages 
    WHERE id = "{int(pageId)}";
    """
    cursor.execute(sql)
    if cursor.rowcount == 0:
        return None
    return cursor.fetchone()[0]

def getLinks(fromPageId):
    sql = f"""
    SELECT toPageId FROM links 
    WHERE fromPageId = {int(fromPageId)};  
    """
    cursor.execute(sql)
    if cursor.rowcount == 0:
        return None
    return [row[0] for row in cursor.fetchall()]

def searchBreadth(targetPageId, currentPageId, depth, nodes):
    if nodes is None or len(nodes) == 0:
        return None
    if depth <= 0:
        for node in nodes:
            if node == targetPageId:
                return [node]
        return None

    for node in nodes:
        found = searchBreadth(targetPageId, node, depth-1, getLinks(node))
        if found is not None:
            found.append(currentPageId)
            return found
    return None


if __name__ == '__main__':
    start = time.time()

    nodes = getLinks(1) # '/wiki/魚津市'
    sql = "SELECT count(id) FROM pages;"
    cursor.execute(sql)
    rowcount = cursor.fetchone()[0]
    targetPageId = 692349  # random.randint(2, rowcount)
    for i in range(0,5):
        found = searchBreadth(targetPageId, 1, i, nodes)
        if found is not None:
            for node in found[::-1]:
                print(getUrl(node))
            break

    cursor.close()
    connector.close()

    t = time.time() - start
    print(f'{t//3600:.0f}:{(t%3600)//60:0>2.0f}:{t%60:0>2.0f}')

