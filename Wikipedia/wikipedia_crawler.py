# coding: UTF-8

import random
import re
import time
from datetime import datetime
from urllib.parse import quote, unquote
import dateutil.parser

import requests
from bs4 import BeautifulSoup

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0'}
base_url = 'https://ja.wikipedia.org'
random_url = quote('/wiki/特別:おまかせ表示')
target_url = quote('/wiki/魚津市')
n_layer = 6
n_trial = 30
cnt_getLinks = 0


def main():
    random.seed(datetime.now())
    # アクセスするURL
    url = random_url
    title, urls, _ = getLinks(url)
    (title_0, urls_0) = (title, urls)
    tracert = [title_0]
    print(title_0, flush=True)
    cnt_trial = 0

    while target_url not in urls:
        url = tuple(urls)[random.randint(0, len(urls) - 1)]
        title, urls, _ = getLinks(url)
        # 同じページに戻った場合は循環経路を削除してから再開
        if title in tracert:
            i = tracert.index(title)
            print(f'({i}:{len(tracert[i:])})', end=' ', flush=True)
            del tracert[i:]
        tracert.append(title)
        # ６層目までに見つからない場合は１層目に戻る
        if len(tracert) == n_layer:
            urls = urls_0
            tracert = [title_0]
            # n回繰り返しても見つからない場合は強制終了
            cnt_trial += 1
            print(cnt_trial, end=' ', flush=True)
            if cnt_trial >= n_trial:
                print('\nNot found')
                return

    tracert.append(unquote(target_url)[6:])
    print()
    print(tracert)
    return


def getLinks(url: str):
    global cnt_getLinks
    cnt_getLinks += 1
    html = requests.get(base_url + url, timeout=1, headers=headers)
    soup = BeautifulSoup(html.text, 'html.parser')
    title = soup.find('h1', {'id': 'firstHeading'}).get_text()
    links = soup.find('div', {'id': 'bodyContent'}) \
        .findAll('a', href=re.compile('^(/wiki/)[^:]*$'))
    urls = [(link.attrs['href']+'#') for link in links]
    urls = set(url[:url.find('#')] for url in urls)
    modified_at = get_datetime_modified(soup)

    return title, urls, modified_at


def get_datetime_modified(soup):
    modified_at = soup.find('li', {'id': 'footer-info-lastmod'}).get_text()
    pattern = re.compile(r'([0-9]{4})年([0-9]{1,2})月([0-9]{1,2})日 \(.\) ([0-9]{2}:[0-9]{2})')
    mOB = re.search(pattern, modified_at)
    datetime = f'{mOB.group(1)}-{mOB.group(2):0>2}-{mOB.group(3):0>2} {mOB.group(4)}:00'

    return datetime


if __name__ == '__main__':
    # start = time.time()
    # main()
    # elapsed_time = time.time() - start
    # print(f'{cnt_getLinks} times, {elapsed_time:.1f} s -> {elapsed_time/cnt_getLinks:.2f} s/access')

    url = quote('/wiki/魚津市立片貝小学校')
    title, urls, modified_at = getLinks(url)
    for url in urls:
        print(unquote(url))
    print(len(urls), "links")
    # print(modified_at)

