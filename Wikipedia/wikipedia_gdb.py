#!/usr/bin/env python
#coding: UTF-8

from urllib.parse import quote

from neo4jrestclient.client import GraphDatabase

from Wikipedia.wikipedia_crawler import getLinks

gdb_url = "http://neo4j:root@localhost:7474/db/data/"
gdb = GraphDatabase(gdb_url)

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0'}
base_url = 'https://ja.wikipedia.org'
source_url = quote('/wiki/魚津市立片貝小学校')


def main():
    nodes = {}
    url = source_url
    title, urls, modified_at = getLinks(url)
    source = gdb.nodes.create(name=title, modified_at=modified_at)
    source.labels.add('0-deg')
    nodes[url] = source

    for url in urls:
        title, _, modified_at = getLinks(url)
        target = gdb.nodes.create(name=title, modified_at=modified_at)
        target.labels.add('1-deg')
        nodes[url] = target
        source.relationships.create("Links", target)
        print(title, flush=True)

    for src, node in nodes.items():
        _, urls, _ = getLinks(src)
        for url in urls:
            if url in nodes.keys():
                node.relationships.create("Links", nodes[url])

    return True


if __name__ == '__main__':
    main()
