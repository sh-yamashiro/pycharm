# -*- coding: utf-8 -*-
"""
@citation:  https://unoh.github.io/2007/06/13/python_2.html
"""

import smtplib
from email.mime.text import MIMEText
from email.utils import formatdate

def create_message(from_addr, to_addr, subject, body):
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Date'] = formatdate()
    return msg

def send_via_gmail(from_addr, to_addr, msg):
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login('ubunsuke@gmail.com', 'pas.ubunsuke')   # 'yourname@gmail.com', 'password')
    s.sendmail(from_addr, [to_addr], msg.as_string())
    s.close()

def send_nortification(subject, body):
    from_addr = 'ubunsuke@gmail.com'
    to_addr = 'yamaaasuke_311@yahoo.co.jp'
    # subject = ''
    # body = ''
    msg = create_message(from_addr, to_addr, subject, body)
    send_via_gmail(from_addr, to_addr, msg)

if __name__ == '__main__':
    from_addr = 'ubunsuke@gmail.com'
    to_addr = 'yamaaasuke-zzz@yahoo.co.jp'  # 'egg@example.com'
    msg = create_message(from_addr, to_addr, 'test subject', 'test body')
    send_via_gmail(from_addr, to_addr, msg)