# coding: UTF-8

import datetime
import re
import time
from urllib.parse import unquote

import mysql.connector
import requests
from bs4 import BeautifulSoup

from mailer import send_nortification

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0'}
base_url = 'https://ja.wikipedia.org'
target_url = '/wiki/魚津市'

connector = mysql.connector.connect(
    host="localhost", user="root", passwd="root", db="wikipedia", charset="utf8mb4")
cursor = connector.cursor(buffered=True)

pages = set()
pages_lv0 = set()
cnt_lv0 = 0
start = 0

def insertPageIfNotExists(url):
    url = url.replace('"', r'\"')
    sql = f"""
    SELECT * FROM pages 
    WHERE url = "{url}";
    """
    cursor.execute(sql)
    if cursor.rowcount == 0:
        sql = f"""
        INSERT INTO pages (url) VALUES ("{url}");
        """
        cursor.execute(sql)
        connector.commit()
        return cursor.lastrowid
    else:
        return cursor.fetchone()[0]

def insertLink(fromPageId, toPageId):
    sql = f"""
    SELECT * FROM links 
    WHERE fromPageId = {int(fromPageId)} AND toPageId = {int(toPageId)};  
    """
    cursor.execute(sql)
    if cursor.rowcount == 0:
        sql = f"""
        INSERT INTO links (fromPageId, toPageId) 
        VALUES ({int(fromPageId)}, {int(toPageId)});
        """
        cursor.execute(sql)
        connector.commit()

def getLinks(url, recLevel):
    global pages, pages_lv0, cnt_lv0, start
    if recLevel > 4:
        return

    pages.add(url)
    pageId = insertPageIfNotExists(url)
    if url in pages_lv0:
        cnt_lv0 += 1
        print(elapsed_time(start), f'( {cnt_lv0} / {len(pages_lv0)} )', url, flush=True)
        sql = f"""
        INSERT INTO buf (pageId) VALUES ({pageId});
        """
        cursor.execute(sql)
        connector.commit()

    html = requests.get(base_url + url, timeout=30, headers=headers)
    soup = BeautifulSoup(html.text, 'html.parser')
    links = soup.find('div', {'id': 'bodyContent'}).findAll(
        'a', href=re.compile('^(/wiki/)[^:]*$'))
    links = [(unquote(link.attrs['href'])+'#') for link in links]
    links = set(url[:url.find('#')] for url in links)

    if recLevel == 0:
        # set_pages_lv0(links)
        pages_lv0 = links
    if is_already_getLinked(url, len(links)):
        return

    for href in links:
        # 上(81)でlinksをひらいてないとき
        # href = unquote(link.attrs['href'])
        # if '#' in href:
        #     href = href[:href.find('#')]
        insertLink(pageId, insertPageIfNotExists(href))
        if href not in pages:
            getLinks(href, recLevel+1)

def is_already_getLinked(url, len_links):
    pageId = insertPageIfNotExists(url)
    sql = f"""
    SELECT * FROM links 
    WHERE fromPageId = {int(pageId)};  
    """
    cursor.execute(sql)
    if cursor.rowcount == len_links:
        return True
    return False

def create_tables():
    sqls = (
        """
        CREATE TABLE IF NOT EXISTS
        wikipedia.pages(
            id BIGINT PRIMARY KEY AUTO_INCREMENT,
            url VARCHAR(255) NOT NULL,
            created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            INDEX idx_url(url)
        );
        """,
        """
        CREATE TABLE IF NOT EXISTS
            wikipedia.links(
                id BIGINT PRIMARY KEY AUTO_INCREMENT,
                fromPageId INT NULL,
                toPageId INT NULL,
                created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
                INDEX idx_from_to(fromPageId, toPageId)
            );
        """,
        "DROP TABLE IF EXISTS wikipedia.buf;",
        """
        CREATE TABLE IF NOT EXISTS
            wikipedia.buf(
                id BIGINT PRIMARY KEY AUTO_INCREMENT,
                pageId INT NULL, 
                created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            );
        """
    )
    for sql in sqls:
        cursor.execute(sql)
    connector.commit()

def elapsed_time(start):
    t = time.time() - start
    return f'{t//3600:.0f}:{(t%3600)//60:0>2.0f}:{t%60:0>2.0f}'


if __name__ == '__main__':
    start = time.time()
    print(str(datetime.datetime.now())[:19])

    create_tables()
    # set_pages_exist()

    try:
        getLinks(target_url, 0)
        send_nortification('Wiki - Process finished', elapsed_time(start))
    except Exception as e:
        msg = str(e.args[0])
        msg += f'\n\n{elapsed_time(start)} ( {cnt_lv0} / {len(pages_lv0)} )'
        send_nortification('Wiki - Exception notice', msg)
    finally:
        cursor.close()
        connector.close()

    print(elapsed_time(start))


# --- archive ---
def set_pages_lv0(links):
    global pages_lv0
    for link in links:
        href = unquote(link.attrs['href'])
        if '#' in href:
            href = href[:href.find('#')]
        pages_lv0.add(href)

def set_pages_exist():
    global pages
    sql = """
    SELECT pages.url FROM pages 
    WHERE pages.id IN (SELECT fromPageId FROM links GROUP BY fromPageId);
    """     # AND pages.id < 285641;
    cursor.execute(sql)
    rows = cursor.fetchall()
    pages = set(row[0] for row in rows)

def cleanPageId():
    import numpy as np
    idTable = np.arange(1000000)

    # pagesを更新
    print('--- pages ---',flush=True)
    sql = "SELECT * FROM cp_pages;"
    cursor.execute(sql)
    rows = cursor.fetchall()

    for row in rows:
        url = row[1].replace('"', r'\"')
        sql = f"""
        INSERT INTO pages (url, created) 
        VALUES ("{url}", "{row[2]}");
        """
        cursor.execute(sql)
        newId = cursor.lastrowid
        idTable[row[0]] = newId
        if newId % 10000 == 0:
            connector.commit()
            print(newId, flush=True)
    connector.commit()

    # linksを更新
    print('--- links ---',flush=True)
    sql = "SELECT * FROM cp_links;"
    cursor.execute(sql)
    rows = cursor.fetchall()

    for row in rows:
        sql = f"""
        INSERT INTO links (fromPageId, toPageId, created) 
        VALUES ({idTable[row[1]]}, {idTable[row[2]]}, "{row[3]}");
        """
        cursor.execute(sql)
        newId = cursor.lastrowid
        if newId % 100000 == 0:
            connector.commit()
            print(newId, flush=True)
    connector.commit()

    return

