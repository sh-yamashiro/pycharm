import sys
import librosa
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from scipy import signal
import seaborn as sns


def main():
    # args = sys.argv
    dirpath = 'D:/Dropbox/music/DINOSAUR/'
    filenames = ('', '01 - Dinosaur.mp3', '02 - CHAMP.mp3', '03 - Still Alive.mp3', '04 - ハルカ.mp3',
                 '05 - それでもやっぱり.mp3', '06 - 声明.mp3', '07 - Queen Of The Night.mp3', '08 - SKYROCKET.mp3',
                 '09 - ルーフトップ.mp3', '10 - 弱い男.mp3', '11 - 愛しき幽霊.mp3', '12 - King Of The Street.mp3',
                 '13 - Purple Pink Orange.mp3')

    args = filenames[:6]
    specs = []
    for filename in args[1:]:
        y, sr = librosa.load(dirpath+filename)  # 音声ファイルの読み込み
        print("loaded:", filename)
        totaltime = len(y) / sr  # 動画の時間(秒)
        time = np.arange(0, totaltime, 1 / sr)  # 時間の配列を作成
        mpl.rcParams["agg.path.chunksize"] = 100000  # データ数が多く、overflowErrorが出た場合これを入れるといいみたいです
        # plt.plot(time,y)
        # plt.title(filename)

        freq, spec = plot_spectrum(y, sr, filename)
        plt.plot(freq, np.abs(spec), label=filename, alpha=0.3)
        # spectrogram(y, sr, filename)

    plt.yscale("log")
    plt.legend()

    plt.show()

def plot_spectrum(y, sr, filename):
    spec = np.fft.fft(y)
    np.fft.fftfreq(y.shape[0], 1.0 / sr)
    freq = np.fft.fftfreq(y.shape[0], 1.0 / sr)
    freq = freq[:int(freq.shape[0] / 2)]
    spec = spec[:int(spec.shape[0] / 2)]
    spec[0] = spec[0] / 2

    # plt.figure()
    # plt.plot(freq, np.abs(spec), label=filename)
    # plt.yscale("log")
    return freq, spec

def spectrogram(y, sr, filename, savefig:bool=False):
    fs = sr  # サンプリング周波数
    f, t, Sxx = signal.spectrogram(y, fs, nperseg=512)

    plt.figure()
    plt.pcolormesh(t, f, Sxx, vmax=1e-6, cmap='viridis')   # cmap='RdYlGn_r')
    plt.xlabel("time [sec]")
    plt.ylabel("freq [Hz]")
    plt.title(filename)
    plt.colorbar()
    if savefig:
        plt.savefig(filename.replace('.mp3', '.png'))


if __name__ == '__main__':
    main()
