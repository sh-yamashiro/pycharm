# -*- coding: utf-8 -*-
import math

def main(x):
    xi = math.floor(x)  # 整数部分 integer
    xf = x - xi         # 小数部分 fraction
    error_min = 0.05

    print(x)
    print('+-{0}-+-{1}-+-{1}-+-{1}-+'.format('-'*8, '-'*6))
    print('| {0:^8} | {1:^6} | {2:^6} | {3:^6} |'.format(' a / b', 'approx', 'error', 'factor'))
    print('+-{0}-+-{1}-+-{1}-+-{1}-+'.format('-'*8, '-'*6))

    for b in range(1, 100):
        af = round(xf * b)  # 近似値の小数部分 || xf ~ af/b
        error = abs(xf - af / b)    # roundによる誤差
        if error < error_min:
            error_min = error
            a = xi * b + af
            factor = x / (a / b)
            print(f'| {a:>3} / {b:>2} | {a/b:.4f} | {error:.4f} | {factor:.4f} |')

    print('+-{0}-+-{1}-+-{1}-+-{1}-+'.format('-'*8, '-'*6))

if __name__ == '__main__':
    n = 5
    print(f'sqrt({n}) = ')
    n = math.sqrt(n)

    # n = math.pi
    # n = float(input('Enter a number >> '))
    main(n)
