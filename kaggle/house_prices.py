#!/usr/bin/env python
#coding: UTF-8

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.metrics import r2_score
import tqdm


# https://pythondatascience.plavox.info/scikit-learn/トレーニングデータとテストデータ
# https://pythondatascience.plavox.info/scikit-learn/線形回帰
# https://pythondatascience.plavox.info/scikit-learn/回帰モデルの評価

def main():
    sns.set_style('white')

    # --- DataFrameの読み込み・整形 ---
    df = pd.read_csv('input/train.csv')
    df_sub = pd.read_csv('input/test.csv')

    # データの連結
    df_concat = pd.concat([df, df_sub])

    # nullを0で置き換え
    for key in df_concat.columns[df_concat.isnull().sum() != 0]:
        df_concat.fillna(value={key: 0}, inplace=True)

    # テキストデータが入っている列全てをdummy化する
    dummy_concat = pd.get_dummies(df_concat, drop_first=True)

    # データの分割
    dummy_train = dummy_concat[:df.shape[0]]
    print('train: ', dummy_train.shape)
    dummy_test = dummy_concat[df.shape[0]:]
    print('test: ', dummy_test.shape)

    # === trainデータでの訓練 ===
    # --- L2正則化パラメータの検討 ---
    """
    # トレーニングデータとテストデータをランダムに分ける
    dummy_train_0, dummy_train_1 = train_test_split(dummy_train, test_size=0.2)

    pred_score = []
    alphas = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30, 100, 300, 1000]
    for alpha in alphas:
        # 線型重回帰分析：共線性の問題を回避するためL2正則化
        clf = linear_model.Ridge(alpha)

        # 説明変数
        df_wo_price = dummy_train_0.drop(['Id', 'SalePrice'], axis=1)
        X = df_wo_price.as_matrix()

        # 目的変数
        Y = dummy_train_0['SalePrice'].as_matrix()

        # 予測モデルを作成
        clf.fit(X, Y)

       # 回帰モデルの結果を評価する
        from sklearn.metrics import r2_score
        y0_true = dummy_train_0['SalePrice']
        y0_pred = dummy_train_0.drop(['Id', 'SalePrice'], axis=1).dot(clf.coef_) + clf.intercept_
        y1_true = dummy_train_1['SalePrice']
        y1_pred = dummy_train_1.drop(['Id', 'SalePrice'], axis=1).dot(clf.coef_) + clf.intercept_

        pred_score.append((r2_score(y0_true, y0_pred), r2_score(y1_true, y1_pred)))

    pd.DataFrame(pred_score, index=alphas).plot()
    plt.semilogx()
    plt.show()
    """

    # --- 線形回帰 ---
    coef_all = []
    icept_all = []
    r2score_all = []
    n_iter = 100
    for i in tqdm.tqdm(range(n_iter)):
        # トレーニングデータとテストデータをランダムに分ける
        dummy_train_0, dummy_train_1 = train_test_split(dummy_train, test_size=0.2)

        # 線型重回帰分析：共線性の問題を回避するためL2正則化
        clf = linear_model.Ridge(10)

        # 説明変数
        df_wo_price = dummy_train_0.drop(['Id', 'SalePrice'], axis=1)
        X = df_wo_price.as_matrix()

        # 目的変数
        Y = dummy_train_0['SalePrice'].as_matrix()

        # 予測モデルを作成
        clf.fit(X, Y)

        # 回帰モデルの結果を評価する
        y_true = dummy_train_1['SalePrice']
        y_pred = dummy_train_1.drop(['Id', 'SalePrice'], axis=1).dot(clf.coef_) + clf.intercept_
        r2score_all.append(r2_score(y_true, y_pred))

        # 結果の更新
        coef_all.append(np.array(clf.coef_))
        icept_all.append(clf.intercept_)

    # --- 結果の集計 ---
    coef_all = pd.DataFrame(coef_all)
    icept_all = pd.DataFrame(icept_all)

    # # 単純平均
    # coef_mean = coef_all.mean().values
    # icept_mean = icept_all.mean()[0]

    # 加重平均
    r2_sum = pd.Series(r2score_all).sum()
    coef_mean = (coef_all.T.dot(r2score_all) / r2_sum).values
    icept_mean = (icept_all.T.dot(r2score_all) / r2_sum)[0]

    # 回帰モデルの結果を評価する
    y_true = dummy_train['SalePrice']
    y_pred = dummy_train.drop(['Id', 'SalePrice'], axis=1).dot(coef_mean) + icept_mean
    r2score = r2_score(y_true, y_pred)
    print('R^2: ', r2score)
    pd.DataFrame(r2score_all).plot()
    plt.axhline(r2score, color='r')
    plt.show()

    # === testデータの予測、提出用データの出力 ===
    # testデータの予測
    y_pred = pd.Series(dummy_test.drop(['Id', 'SalePrice'], axis=1).dot(coef_mean) + icept_mean, name='SalePrice')
    y_pred = pd.concat([dummy_test.Id, y_pred], axis=1)

    # y_pred.to_csv('output/submission_03.csv', encoding='utf-8', index=False)


if __name__ == '__main__':

    main()
