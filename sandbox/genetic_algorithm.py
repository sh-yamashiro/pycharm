# -+- coding: utf-8 -*-
import random  # ランダムモジュール
import math


def geneticoptimize(popnum=6, popsize=10, step=1, mutprob=0.2, elite=0.2, maxiter=500):
    # 突然変異の操作
    def mutate(vec):
        i = random.randint(0, popnum - 1)
        return vec[0:i] + [random.randint(0, 9)] + vec[i + 1:]

    # 交叉の操作
    def crossover(r1, r2):
        i = random.randint(1, popsize - 2)
        return r1[0:i] + r2[i:]

    # 初期化個体群の構築
    pop = []
    for i in range(popsize):
        vec = [random.randint(0, 9) for v in range(0, popnum)]
        pop.append(vec)

    # 各世代のエリート数
    topelite = int(elite * popsize)

    # メインループ
    scores = []
    for i in range(maxiter):
        for vec in pop:
            x = convertn(vec)
            scores.append([costf(x), vec])
        scores.sort()
        ranked = [v for (s, v) in scores]

        # 結果が良かったものをエリートとする。
        pop = ranked[0:topelite]

        # エリートから突然変異や交配を行ったものを作成し、
        # 新しい個体群（次の世代）を生成する。
        while len(pop) < popsize:
            if random.random() < mutprob:

                # 突然変異
                c = random.randint(0, topelite)
                pop.append(mutate(ranked[c]))

            else:

                # 交叉
                c1 = random.randint(0, topelite)
                c2 = random.randint(0, topelite)
                pop.append(crossover(ranked[c1], ranked[c2]))

    return convertn(scores[0][1])


def costf(x):
    return (3 * (x ** 4)) - (5 * (x ** 3)) + (2 * (x ** 2))


def convertn(vec):
    x, y = 0, 0
    for v in vec:
        x += v * (10 ** y)
        y -= 1
    return x


if __name__ == '__main__':
    success, failure = 0, 0
    for i in range(0, 100):
        ans = geneticoptimize()
        if (ans >= -0.05 and ans <= 0.05):
            failure += 1
        if (ans > 0.8 and ans <= 0.9):
            success += 1

    print(success, failure)
