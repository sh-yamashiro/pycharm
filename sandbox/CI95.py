# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import math
from scipy import stats


n = 10  # サンプルサイズ
repeat = 917    # 標本抽出の繰り返し数
t95 = stats.t.ppf(1 - (1 - 0.95) / 2, n - 1)    # 自由度n-1のt分布
t99 = stats.t.ppf(1 - (1 - 0.99) / 2, n - 1)
print(f'95% - {t95:.2f}, 99% - {t99:.2f}')


def main(one_trial: bool=False):
    z = []  # [x[], mean[], ci95[], ci99[]]
    for i in range(repeat):
        population = np.random.normal(size=n)
        sd = population.std(ddof=1)     # delta degree of freedom: 不偏標準偏差を計算
        z.append([population.mean(), t95 * sd / math.sqrt(n), t99 * sd / math.sqrt(n)])

    z.sort()
    x = [np.arange(repeat)]
    z = np.concatenate((x, np.array(z).T))  # ndarrayの連結

    cz = z[:, abs(z[1, :]) > z[3, :]]   # 99%CI の外
    rz = z[:, abs(z[1, :]) > z[2, :]]   # 95%CI の外
    kz = z[:, abs(z[1, :]) <= z[2, :]]  # 95%CI の区間内
    p = len(rz[0, :]) / repeat
    if not one_trial:
        return p

    # 散布図_横
    if repeat > 100:
        plt.scatter(kz[1, :], kz[0, :], color='none', marker='o', s=1, facecolors='w', zorder=3)
    else:
        plt.scatter(kz[1, :], kz[0, :], color='k', marker='o', facecolors='none', zorder=3)
    plt.scatter(rz[1, :], rz[0, :], color='r', marker='o', facecolors='none', zorder=3)
    plt.scatter(cz[1, :], cz[0, :], color='r', marker='o', zorder=4)
    errorbar = {'capsize': 3, 'capthick': 1, 'fillstyle': 'none', 'linestyle': 'None'}
    plt.errorbar(z[1, :], z[0, :], xerr=z[2, :], ecolor='k', zorder=2, **errorbar)
    plt.errorbar(z[1, :], z[0, :], xerr=z[3, :], ecolor='grey', zorder=1, **errorbar)
    # plt.ylim([-0.5, 0.5])
    plt.axvline(0, c='r', zorder=5)
    plt.tick_params(left=False, labelleft=False)
    plt.title(f'alpha error: {p*100:.2f}% ({len(rz[0, :])} / {repeat}) (N={n})')

    plt.show()


if __name__ == '__main__':
    # main(one_trial=True)
    # exit()
    ps = []
    for j in range(100):
        ps.append(main())
    mu = np.mean(ps)
    plt.plot(ps, 'ko')
    plt.axhline(mu, c='r')
    plt.title(f'mean: {mu*100:.2f}% (N={n})')
    plt.show()
