# -*- coding:utf-8 -*-
from matplotlib import pyplot as plt
from matplotlib import animation
import numpy as np
import matplotlib.cm as cm

fig = plt.figure()
plt.xlim([0, 100])
plt.ylim([-3, 3])

ims = []

for i in range(10):
        myColor = cm.gist_rainbow(i / 10)
        rand = np.random.randn(100)     # 100個の乱数を生成
        im = plt.plot(rand, color=myColor)             # 乱数をグラフにする
        ims.append(im)                  # グラフを配列 ims に追加

# 10枚のプロットを 100ms ごとに表示
ani = animation.ArtistAnimation(fig, ims, interval=100)
ani.save('animation.gif', writer='imagemagick')
plt.show()
