# -+- coding: utf-8 -*-
import random
import re


def op(a, b):
    if a is None or b is None:
        return [None, None, None, None]
    elif b == 0:
        return [a + b, a - b, a * b, None]
    else:
        return [a + b, a - b, a * b, a / b]


def showEq(q4: list, p: tuple, order: str):
    eq = order
    opList = ('+', '-', '*', '/')

    for i in range(4):
        eq = eq.replace(f'q{i}', f'{q4[i]}')
    for i in range(3):
        eq = eq.replace(f'p{i}', f'{opList[p[i]]}')

    return eq


def searchAnswer(question: str, sum: int, nPrintLine:int=99):
    q4 = [int(question[i:(i + 1)]) for i in range(dig)]
    cnt = 0

    # 1, ((a + b) + c) + d
    order = '((q0 p0 q1) p1 q2) p2 q3'
    q3 = [op(q4[0], q4[1]), [q4[2]], [q4[3]]]
    q2 = [[op(q3[0][i], q3[1][0]) for i in range(dig)], [q3[2][0]]]
    q1 = [[op(q2[0][i][j], q2[1][0]) for j in range(dig)] for i in range(dig)]

    for i, v in enumerate(q1):
        for j, w in enumerate(v):
            for k, x in enumerate(w):
                if x is None:
                    continue
                if round(x, 3) == sum:
                    if cnt < nPrintLine:
                        print(f'{showEq(q4, (i, j, k), order)} = {round(x)}')
                    cnt += 1

    # 2, (a + (b + c)) + d
    order = '(q0 p0 (q1 p1 q2)) p2 q3'
    q3 = [[q4[0]], op(q4[1], q4[2]), [q4[3]]]
    q2 = [[op(q3[0][0], q3[1][i]) for i in range(dig)], [q3[2][0]]]
    q1 = [[op(q2[0][i][j], q2[1][0]) for j in range(dig)] for i in range(dig)]

    for i, v in enumerate(q1):
        for j, w in enumerate(v):
            for k, x in enumerate(w):
                if x is None:
                    continue
                if round(x, 3) == sum:
                    if cnt < nPrintLine:
                        print(f'{showEq(q4, (j, i, k), order)} = {round(x)}')
                    cnt += 1

    # 3, a + ((b + c) + d)
    order = 'q0 p0 ((q1 p1 q2) p2 q3)'
    q3 = [[q4[0]], op(q4[1], q4[2]), [q4[3]]]
    q2 = [[q3[0][0]], [op(q3[1][i], q3[2][0]) for i in range(dig)]]
    q1 = [[op(q2[0][0], q2[1][i][j]) for j in range(dig)] for i in range(dig)]

    for i, v in enumerate(q1):
        for j, w in enumerate(v):
            for k, x in enumerate(w):
                if x is None:
                    continue
                if round(x, 3) == sum:
                    if cnt < nPrintLine:
                        print(f'{showEq(q4, (k, i, j), order)} = {round(x)}')
                    cnt += 1

    # 4, a + (b + (c + d))
    order = 'q0 p0 (q1 p1 (q2 p2 q3))'
    q3 = [[q4[0]], [q4[1]], op(q4[2], q4[3])]
    q2 = [[q3[0][0]], [op(q3[1][0], q3[2][i]) for i in range(dig)]]
    q1 = [[op(q2[0][0], q2[1][i][j]) for j in range(dig)] for i in range(dig)]

    for i, v in enumerate(q1):
        for j, w in enumerate(v):
            for k, x in enumerate(w):
                if x is None:
                    continue
                if round(x, 3) == sum:
                    if cnt < nPrintLine:
                        print(f'{showEq(q4, (k, j, i), order)} = {round(x)}')
                    cnt += 1

    # 5, (a + b) + (c + d)
    order = '(q0 p0 q1) p1 (q2 p2 q3)'
    q2 = [op(q4[0], q4[1]), op(q4[2], q4[3])]
    q1 = [[op(q2[0][i], q2[1][j]) for j in range(dig)] for i in range(dig)]

    for i, v in enumerate(q1):
        for j, w in enumerate(v):
            for k, x in enumerate(w):
                if x is None:
                    continue
                if round(x, 3) == sum:
                    if cnt < nPrintLine:
                        print(f'{showEq(q4, (i, k, j), order)} = {round(x)}')
                    cnt += 1

    return cnt


if __name__ == '__main__':
    dig = 4     # ケタを変えると解けない
    sum = 10
    doRepeat = False
    pattern = re.compile(r'^[0-9]{4}$')

    while True:
        question = input(f'{dig}ケタの数字を入力 >> ')
        while re.match(pattern, question) is None:
            if question.startswith('exit') or question.startswith('q'):
                exit()

            elif question.startswith('s'):
                question = question.replace(' ', '')
                if '=' in question:
                    sum = int(question[question.index('=') + 1:])
                    doRepeat = False
                print(f'sum is {sum}.')

            elif question.startswith('r'):
                question = f'{random.randint(0, 9999):0>4}'
                break

            elif question.startswith('0to10'):
                sum = range(0, 11)
                doRepeat = True

            question = input(f'{dig}ケタの数字を入力 >> ')

        print(f'question: {question}')

        if doRepeat:
            cntRep = 0
            for n in sum:
                if searchAnswer(question, n, 1) == 0:
                    print(f'{n:>2}: Not found.')
                else:
                    cntRep += 1
            print(f' {cntRep} / 11 answers found.')

        else:
            if searchAnswer(question, sum) == 0:
                print('Answer not found.')
        print('')

    # f = open('list.txt', 'w')
    # for i in range(10000):
    #     question = f'{i:0>4}'
    #     cntRep = 0
    #     str = f'{question}\t'
    #     for sum in range(0, 11):
    #         x = searchAnswer(question, sum, 0)
    #         str += f'{x}\t'
    #         if x > 0:
    #             cntRep += 1
    #
    #     f.write(f'{str}{cntRep}\n')
    # f.close()
