# coding: UTF-8

import re
import time
from datetime import datetime
import dateutil.parser
import zipfile

import requests
from bs4 import BeautifulSoup

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0'}
base_url = 'https://www.saichu.jp'
file_dir = 'C:\\Users\\VAIO_PC\\Downloads\\'
dept_dict = {
    '循環器内科': '/department/circulatory_organ/',
    '心臓血管外科': '/department/heart_blood_vessel/',
    '消化器内科': '/department/gastroenterological_medicine/',
    '一般・消化器外科': '/department/gastrointestinal_surgery/',
    '腎臓内科': '/department/kidney_internal_medicine/',
    '泌尿器科': '/department/urology_department/',
    '呼吸器内科': '/department/pulmonology_medicine/',
    '呼吸器外科': '/department/respiratory_surgery/',
    '神経内科': '/department/neurology/',
    '脳神経外科': '/department/neurosurgery/',
    '脳血管内治療科': '/department/treatment_cerebral_blood_vessel/',
    '精神科(心療科)': '/department/psychosomatic_medicine/',
    '産婦人科': '/department/gynecology/',
    '乳腺外科': '/department/breast_internal_secretion/',
    '腫瘍内科': '/department/tumor_medicine/',
    '緩和ケア科': '/department/mild_care/',
    '放射線治療科': '/department/radiation_therapy/',
    '救急診療科（救命救急センター）': '/department/emergency_clinical_department/',
    '小児科': '/department/pediatrics/',
    '総合診療・感染症内科': '/department/internalmedicine/',
    '血液内科': '/department/blood_tumor_infectious-disease/',
    'リウマチ･膠原病内科': '/department/rheumatism/',
    '糖尿病・内分泌内科': '/department/diabetes_internal_secretion/',
    '整形外科': '/department/orthopedics/',
    '形成外科': '/department/plastic_surgery/',
    '眼科': '/department/ophthalmology/',
    '耳鼻咽喉科': '/department/nose_ears-throat/',
    '皮膚科': '/department/skin/',
    '歯科・口腔外科': '/department/dentistry_oral-surgery/',
    '放射線科': '/department/radiology/',
    '麻酔科': '/department/anesthesia/',
    '病理診断科': '/department/pathological_diagnosis/',
    '臨床検査医学科': '/department/clinical_test_medicine/',
    'リハビリテーション科': '/department/rehabilitation/'
}

def main():
    with open(file_dir+'済生会中央病院ドクター.csv', 'w') as f:

        for dept, url in dept_dict.items():
            print(dept, flush=True)
            html = requests.post(base_url + url + 'doctor/', headers=headers)
            soup = BeautifulSoup(html.text, 'html.parser')

            docs = soup.find('div', {'class': 'content-inner'}).findAll('dt')
            for doc in docs:
                doc_str = doc.text
                # if doc_str is None:
                #     doc_str = doc.contents[1]
                doc_str = doc_str.replace('\n', '').replace('\r', '').replace('\t', '')
                matchOB = re.match(r'(.+)　([^　]+)', doc_str)
                if matchOB is None:
                    doc_name = doc_str
                    doc_pos = '-'
                else:
                    doc_name = matchOB.group(1)
                    doc_pos = matchOB.group(2)

                f.write(','.join([dept, doc_name, doc_pos]) + '\n')

                # exit()

        f.close()


if __name__ == '__main__':
    main()
