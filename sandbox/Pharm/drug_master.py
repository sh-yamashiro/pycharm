# coding: UTF-8
"""
社会保険診療報酬支払基金 HPから医薬品マスタを取得
"""
import re
import requests
from bs4 import BeautifulSoup
import time
import pandas as pd
import csv

time_flag = True
BASE_URL = "http://www.ssk.or.jp/"


def main():
    # アクセスするURL
    url = "http://www.ssk.or.jp/seikyushiharai/tensuhyo/kihonmasta/kihonmasta_04.html"

    # URLにアクセスする htmlが帰ってくる
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0'}
    html = requests.get(url, timeout=1, headers=headers)

    # htmlをBeautifulSoupで扱う
    soup = BeautifulSoup(html.content, "html.parser")

    # タイトルを含むh1要素を取り出し > リンクを取り出し
    update = soup.find("h1", id="pageTitle").string
    pattern = re.compile(r'..[0-9]{1,2}年[0-9]{1,2}月[0-9]{1,2}日')
    update = re.search(pattern, update).group(0)

    # a要素を取り出し > URLを抽出
    download_urls = []
    pattern = re.compile(r'[1-4]\.xls$')
    links = soup.find_all('a')
    for link in links:
        href = link.get('href')
        if href and re.search(pattern, href) is not None:
            download_urls.append(href)

    # ファイルのダウンロード
    for download_url in download_urls:
        # 一秒スリープ
        time.sleep(1)

        file_name = download_url.split("/")[-1]
        if BASE_URL in download_url:
            r = requests.get(download_url)
        else:
            r = requests.get(BASE_URL + download_url)

        # ファイルの保存
        if r.status_code == 200:
            f = open(file_name, 'wb')
            f.write(r.content)
            f.close()

    return True


def read_xls(file_name):
    list = ["tp20171208-01_1.xls", "tp20171208-01_2.xls", "tp20171208-01_3.xls", "tp20170215-01_4.xls"]
    f = pd.ExcelFile(list[0])

    df = f.parse(f.sheet_names[0])

    f.close()


if __name__ == '__main__':
    main()
    # read_xls('a')

