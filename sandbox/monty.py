# -*- coding: utf-8 -*-

from scipy.stats import norm
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import random
# import seaborn as sns


def ppv(sensitivity: float, specificity: float, prevalence: float):
    """
    positive predictive value: 陽性的中率
    :param sensitivity: 感度
    :param specificity: 特異度
    :param prevalence: 有病率
    :return:
    """
    return sensitivity * prevalence / (sensitivity * prevalence + (1 - specificity) * (1 - prevalence))


def simulate_ppv():
    sensitivity = 0.60
    specificity = 0.99
    prevalence = np.array([0.05, 0.01, 0.001, 0.0001])
    value = ppv(sensitivity, specificity, prevalence)

    for p, v in zip(prevalence, value):
        print(f'{p*100}%:\t{v:.3}')

    plt.plot(prevalence, value)
    plt.semilogx()
    plt.show()


def monty_hall(case: int):
    """
    1.  Participant chooses door A.
    2.  Monty opens either door B or door C which is not the hit.
        When both door B and door C are not the hit, he opens one at random.
    3.  Participant changes the choice.
    4.  Participant wins when the choice is the hit.
    :return:
    """
    choices = ['A', 'B', 'C']
    hit = choices[random.randint(0, 2)]
    choice = choices[0]

    # 当たりでない方を開ける.
    if case == 1:
        if hit == 'A':
            choices.pop(random.randint(1, 2))
        elif hit == 'B':
            choices.remove('C')
        elif hit == 'C':
            choices.remove('B')
    # ランダムに開ける. 当たりの場合は負け.
    if case == 2:
        choices.pop(random.randint(1, 2))
        if hit not in choices:
            return None

    choice = choices[1]

    if choice == hit:
        return 1
    else:
        return 0


def simulate_monty_hall():
    ans = []
    n = 1000
    for case in (1, 2):
        win = 0
        for i in range(n):
            result = monty_hall(case)
            if result is None:
                n -= 1
                continue
            win += result
        ans.append(win/n*100)
        # print(f'case{case}: {win} / {n} = {win/n*100:.3}%')
    return ans


def repeat_simulation_monty_hall():
    monty1 = []
    monty2 = []
    n_trial = 500
    for i in range(n_trial):
        buf = simulate_monty_hall()
        monty1.append(buf[0])
        monty2.append(buf[1])

    m, bins, patches = plt.hist(monty1, color='b', normed=1, alpha=0.5)
    param = norm.fit(monty1)
    pdf_fit = norm.pdf(bins, param[0], param[1])
    plt.plot(bins, pdf_fit, 'b--')
    plt.xlabel('Probability')
    title = f'$\mu={param[0]:.4}$, $\sigma={param[1]:.3}$'

    m, bins, patches = plt.hist(monty2, color='g', normed=1, alpha=0.5)
    param = norm.fit(monty2)
    pdf_fit = norm.pdf(bins, param[0], param[1])
    plt.plot(bins, pdf_fit, 'g--')
    plt.xlabel('Probability')
    title += f' / $\mu={param[0]:.4}$, $\sigma={param[1]:.3}$'

    plt.title(title)
    plt.show()


def poincare(n, k):
    mu = 950
    sgm = 50
    # n = 3
    n_trial = 1000
    weights_max = np.ndarray(n_trial)
    for j in range(n_trial):
        weights = np.ndarray(n)
        for i in range(n):
            weights[i] = (random.gauss(mu, sgm))
        weights_max[j] = weights.max()
    mean = weights_max.mean()
    median = np.median(weights_max)

    plt.subplot(4, 1, k)
    num_bins = 50
    m, bins, patches = plt.hist(weights_max, num_bins, normed=1, alpha=0.5)
    param = norm.fit(weights_max)

    pdf_fit = norm.pdf(bins, param[0], param[1])
    plt.plot(bins, pdf_fit, '--')
    plt.xlim(800, 1200)
    # plt.xlabel('weight')
    # plt.ylabel('Probability density')
    # plt.title(f'Histogram: $\mu={param[0]:.5}$, $\sigma={param[1]:.3}$')
    plt.title(f'#{n}: $\mu={mean:.5}$, ' + r'$\mu_{1/2}$' + f'$={median:.5}$')
    return ''


def simulate_poincare():
    # sns.set_style('whitegrid')
    poincare(1, 1)
    poincare(3, 2)
    poincare(5, 3)
    poincare(10, 4)

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    repeat_simulation_monty_hall()
