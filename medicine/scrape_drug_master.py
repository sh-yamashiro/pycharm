# coding: UTF-8
"""
@citation: https://qiita.com/Azunyan1111/items/9b3d16428d2bcc7c9406
@citation: http://vaaaaaanquish.hatenablog.com/entry/2017/06/25/202924
"""
import os
import mysql.connector
import csv
import re
from tqdm import tqdm

root_dir = '/home/yama/iyaku/'

connector = mysql.connector.connect(
    host="localhost", user="root", passwd="root", db="medicine", charset="utf8mb4")
cursor = connector.cursor(buffered=True)

# === データベース作成 ===
def make_database():
    # マスターファイルの名前を取得
    file_list = os.listdir(root_dir)
    file_g = [f for f in file_list if f.startswith('ippanmeishohoumaster') and f.endswith('.csv')][-1]
    file_b1 = [f for f in file_list if f.startswith('tp20') and f.endswith('-01_01.csv')][-1]
    file_b3 = [f for f in file_list if f.startswith('tp20') and f.endswith('-01_03.csv')][-1]

    import_generic_name(root_dir + file_g)
    import_brand_name(root_dir + file_b1)
    import_brand_name(root_dir + file_b3)

    return file_g, file_b1, file_b3

def import_generic_name(uri:str):
    # csv fileの読み込み
    csv_file = open(uri, 'r')
    f = csv.reader(csv_file, delimiter=",", doublequote=True, lineterminator="\n", quotechar='"')
    header = next(f)
    # > [区分, 一般名コード, 一般名処方の標準的な記載, 成分名, 規格, 一般名処方加算対象, 例外コード, 同一剤形・規格内の最低薬価, 備考]

    for row in tqdm(f):
        # 要素の追加 > genericテーブル
        sql = f""" 
        INSERT INTO medicine.generic
        VALUES(
            NULL, '{row[0]}', '{row[1]}', '{row[2]}', '{row[3]}', '{row[4]}', {float(row[7].replace(',', ''))}
        );
        """
        cursor.execute(sql)

        # 配合剤の各成分量を追加 > combiテーブル
        if '・' not in row[3]:
            continue
        name_list = re.match(r'【般】(.+)配合.+', row[2]).group(1)
        name_list = name_list.split('・')
        comp_list = row[3].split('・')

        for comp in comp_list:
            size = ''
            name = [n for n in name_list if n.startswith(comp)]
            if len(name) != 0:
                size = name[0].replace(comp, '')

            # 要素の追加 > combiテーブル
            sql = f""" 
            INSERT INTO medicine.combi
            VALUES(
                NULL, '{row[1]}', '{comp}', '{size}'
            );
            """
            cursor.execute(sql)

    connector.commit()

def import_brand_name(uri: str):
    # csv fileの読み込み
    csv_file = open(uri, 'r')
    f = csv.reader(csv_file, delimiter=",", doublequote=True, lineterminator="\n", quotechar='"')
    header = next(f)
    # > [区分, 薬価基準収載医薬品コード, 成分名, 規格, , , , 品名, メーカー名, 診療報酬において加算等の算定対象となる後発医薬品,
    #    先発医薬品, 同一剤形・規格の後発医薬品がある先発医薬品, 薬価, 経過措置による使用期限, 備考]

    for row in tqdm(f):
        # メーカー名が空欄のものを排除
        if row[8] == "":
            continue
        flag = "b'0', b'0', b'0'"
        # >後発品
        if row[9]:
            flag = "b'0', b'0', b'1'"
        # >先発品
        elif row[10]:
            # >後発品がある先発品
            if row[11]:
                flag = "b'0', b'1', b'0'"
            else:
                flag = "b'1', b'0', b'0'"

        # 要素の追加 > brandテーブル
        sql = f""" 
        INSERT INTO medicine.brand
        VALUES(
            NULL, '{row[0]}', '{row[1]}', '{row[7]}', '{row[8]}', '{row[2]}', '{row[3]}',
             {flag}, {float(row[12].replace(',', ''))}
        );
        """
        cursor.execute(sql)

    connector.commit()

def import_combination_drug(uri: str):
    return

# === MySQLテーブル関連 ===
def format_tables():
    sqls = (
        "DROP TABLE medicine.generic, medicine.brand, medicine.combi;",
        """
        CREATE TABLE IF NOT EXISTS
            medicine.generic(
                id BIGINT PRIMARY KEY AUTO_INCREMENT,
                class TEXT,
                yjcode VARCHAR(12) NOT NULL,
                name TEXT NOT NULL,
                component TEXT NOT NULL,
                size TEXT NOT NULL,
                min_price FLOAT(10, 2) NOT NULL,
                INDEX idx_code(yjcode),
                INDEX idx_name(name(10)),
                INDEX idx_comp(component(10))
            );
        """,
        """
        CREATE TABLE IF NOT EXISTS
            medicine.brand(
                id BIGINT PRIMARY KEY AUTO_INCREMENT,
                class TEXT,
                yjcode VARCHAR(12) NOT NULL,
                name TEXT NOT NULL,
                maker TEXT,
                component TEXT NOT NULL,
                size TEXT NOT NULL,
                is_s bit(1),
                has_g bit(1),
                is_g bit(1),
                price FLOAT(10,2) NOT NULL,
                INDEX idx_code(yjcode),
                INDEX idx_name(name(10)),
                INDEX idx_comp(component(10))
            );
        """,
        """
        CREATE TABLE IF NOT EXISTS
            medicine.combi(
                id BIGINT PRIMARY KEY AUTO_INCREMENT,
                yjcode VARCHAR(12) NOT NULL,
                component TEXT NOT NULL,
                size TEXT NOT NULL,
                INDEX idx_code(yjcode),
                INDEX idx_comp(component(10))
            );
        """
    )
    for sql in sqls:
        cursor.execute(sql)
    connector.commit()

if __name__ == '__main__':
    format_tables()
    make_database()

    cursor.close()
    connector.close()

