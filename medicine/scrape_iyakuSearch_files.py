# coding: UTF-8
"""
@citation: https://qiita.com/Azunyan1111/items/9b3d16428d2bcc7c9406
@citation: http://vaaaaaanquish.hatenablog.com/entry/2017/06/25/202924
"""
import os
import mysql.connector
import re
from tqdm import tqdm
from bs4 import BeautifulSoup
from mojimoji import zen_to_han

time_flag = True
root_dir = '/home/yama/iyaku/'
utf8_dir = '/home/yama/iyaku/utf8/'

connector = mysql.connector.connect(
    host="localhost", user="root", passwd="root", db="medicine", charset="utf8mb4")
cursor = connector.cursor(buffered=True)

def sgm_to_dict(sgm_uri:str):
    # fileを開く
    f = open(sgm_uri, 'r')
    html = f.read()

    # htmlをBeautifulSoupで扱う
    soup = BeautifulSoup(html, "html.parser")

    # === 要素の取得 | medicinesテーブル ===
    med_dics = []
    for yjcode, startdate, brandname, regulatory, composition \
            in zip(get_yjcode_mt(soup), get_startdate_mt(soup), get_brandname_mt(soup),
                   get_regulatory_mt(soup), get_composition_mt(soup)):
        med_dic = {
            'companyid':    get_companyid(soup),
            'yjcode':       yjcode,
            'startdate':    startdate,
            'brandname':    brandname,
            'regulatory':   regulatory,
            'class':        get_class(soup),
            'genericname':  get_genericname(soup),
            'efficacy':     get_efficacy(soup),
            'doseadmin':    get_doseadmin(soup),
            'contra':       get_contraindication(soup),
            'adverse':      get_adverse(soup),
            'composition':  composition,
            'uri':          sgm_uri.replace(utf8_dir, '')
        }
        med_dics.append(med_dic)

    return med_dics

# 特定のclassのみを抽出
def sgm_to_dict_select_by_class(sgm_uri:str, keyword:str=None):
    # fileを開く
    f = open(sgm_uri, 'r')
    html = f.read()

    # htmlをBeautifulSoupで扱う
    soup = BeautifulSoup(html, "html.parser")

    # === 要素の取得 | medicinesテーブル ===
    med_dics = []
    for yjcode, startdate, brandname, regulatory, composition \
            in zip(get_yjcode_mt(soup), get_startdate_mt(soup), get_brandname_mt(soup),
                   get_regulatory_mt(soup), get_composition_mt(soup)):
        med_dic = {
            'companyid':    get_companyid(soup),
            'yjcode':       yjcode,
            'startdate':    startdate,
            'brandname':    brandname,
            'regulatory':   regulatory,
            'class':        get_class(soup),
            'genericname':  get_genericname(soup),
            'efficacy':     get_efficacy(soup),
            'doseadmin':    get_doseadmin(soup),
            'contra':       get_contraindication(soup),
            'adverse':      get_adverse(soup),
            'composition':  composition,
            'uri':          sgm_uri.replace(utf8_dir, '')
        }
        if keyword and keyword in med_dic['class']:
            med_dics.append(med_dic)

    if len(med_dics) == 0:
        return None
    else:
        return med_dics

# === === === 文字列の整形 === === ===
def remove_variablelabel(tag):
    if tag is None:
        return None
    if tag.find("variablelabel"):
        for t in tag.find_all("variablelabel"):
            t.decompose()
    return tag

def strip_(tag, include_enter:bool=True):
    if tag is None:
        return ''
    string = tag.text
    string = zen_to_han(string, kana=False)
    if include_enter:
        pattern = r'\s|&enter;|○|※|＊|\*'
    else:
        pattern = r'\s|○|※|＊|\*'
    return re.sub(pattern, '', string)

# === === === 要素の取得 | medicinesテーブル === === ===
regex_item_detail = re.compile(r'^(item|detail)$')
def get_companyid(soup):
    tag = soup.find("companyidentifier")
    tag = remove_variablelabel(tag)
    return strip_(tag)

def get_yjcode_mt(soup):
    tag = soup.find_all("yjcode")
    tag = [remove_variablelabel(t) for t in tag]
    return [strip_(t) for t in tag]

def get_startdate_mt(soup):
    tag = soup.find_all("startingdateofmarketing")
    if tag is None:
        return [''] * 10

    tag = [t.find("yearmonth") for t in tag]
    tag = [remove_variablelabel(t) for t in tag]
    # tag = [remove_variablelabel(t.find("yearmonth")) for t in tag]
    return [strip_(t) for t in tag]

def get_brandname_mt(soup):
    tag = soup.find_all("approvalbrandname")
    tag = [remove_variablelabel(t) for t in tag]
    return [strip_(t) for t in tag]

def get_regulatory_mt(soup):
    tag = soup.find_all("regulatoryclassification")
    if tag is None:
        return [''] * 10

    tag = [t.find_all("item") for t in tag]
    regs = []
    for t in tag:
        t = [remove_variablelabel(t0) for t0 in t]
        reg = [strip_(t0) for t0 in t]
        # 表記ゆらぎの修正
        reg = [re.sub(r'[、・]', ',', r) for r in reg]
        reg = [re.sub(r'\(?注.+$', '', r) for r in reg]
        reg = [re.sub(r'処方せん医薬品', '処方箋医薬品', r) for r in reg]
        reg = [re.sub(r'医薬品[0-9]?.?', '医薬品', r) for r in reg if r != '']
        # すべて''の場合、regs要素数 -1 を避けるため
        if len(reg) == 0:
            reg = ['']
        regs.append(','.join(reg))
    return regs

def get_class(soup):
    tag = soup.find("therapeuticclassification")
    if tag is None:
        return ''

    tag = tag.find("detail")
    tag = remove_variablelabel(tag)
    cls = strip_(tag)
    # 表記ゆらぎの修正
    cls = re.sub(r'(治療|拮抗|阻害)薬', r'\1剤', cls)
    return cls

def get_genericname(soup):
    if soup.genericname:
        tag = soup.find("genericname")
        tag = tag.find(regex_item_detail)
    elif soup.standardname:
        # 一般的名称の代わりに基準名を使用
        tag = soup.find("standardname")
        tag = tag.find("detail")
    else:
        return ''

    tag = remove_variablelabel(tag)
    return strip_(tag)

def get_efficacy(soup):
    tag = soup.find("indicationsorefficacy")
    if tag is None:
        return ''

    # 複数の用法がある場合 <low1subitem>
    if tag.low1subitem:
        tag = [t.find(regex_item_detail) for t in tag.find_all("low1subitem")]
    else:
        tag = tag.find_all("detail")

    for t in tag:
        if t and t.variablelabel:
            t.variablelabel.decompose()
    tag = [strip_(t) for t in tag if t]
    return ','.join(tag)

# 以下、未refactoring
def get_doseadmin(soup):
    tag = soup.find("doseadmin")
    if tag is None:
        return ''

    tag = tag.find_all("detail")
    for t in tag:
        if t.variablelabel:
            t.variablelabel.decompose()
    tag = [strip_(t) for t in tag]
    return ','.join(tag)

def get_contraindication(soup):
    tag = soup.find("contraindications")
    if tag is None:
        return None

    tag = tag.find_all(regex_item_detail)
    for t in tag:
        if t.variablelabel:
            t.variablelabel.decompose()
    tag = [strip_(t) for t in tag]
    return ','.join(tag)

def get_adverse(soup):
    tag = soup.find("seriousadverse")
    if tag is None:
        return None

    tag = tag.find_all("item")
    for t in tag:
        if t.chem:
            t.chem.decompose()
        if t.variablelabel:
            t.variablelabel.decompose()
    tag = [''.join(t.text) for t in tag]
    return ','.join(tag)

def get_composition_mt(soup):
    tag = soup.find_all("composition")
    if tag is None:
        return [''] * 10

    main_comp = get_genericname(soup)
    main_comp = re.sub(r'([ア-ン]+).+', r'\1', main_comp)
    tag = [t.find_all("detail") for t in tag]
    comps = []
    for t in tag:
        for t0 in t:
            if t0.chem:
                t0.chem.decompose()
        t = [remove_variablelabel(t0) for t0 in t]

        t = [strip_(t0, include_enter=False) for t0 in t]
        t = [t0.split('&enter;') for t0 in t]
        comp = []
        for s in t:
            comp.extend(s)
        # 表記ゆらぎの修正
        if len(main_comp) >= 4:
            comp = [r for r in comp if main_comp not in r]
        comp = [re.sub(r'[,、・]+', ',', r) for r in comp]
        comp = [re.sub(r'^[0-9a-zA-Z.μ]+$', '', r) for r in comp]
        comp = [re.sub(r'添加物として,?', '', r) for r in comp]
        comp = [re.sub(r'.+(次|下記)の成分(を含有)?$', '', r) for r in comp]
        comp = [re.sub(r'(.+)を含有する[.。]?$', r'\1', r) for r in comp if r != '']
        # comp = [re.sub(r'医薬品[0-9]?.?', '医薬品', r) for r in comp if r != '']

        # すべて''の場合、regs要素数 -1 を避けるため
        if len(comp) == 0:
            comp = ['']
        comps.append(';'.join(comp))
    return comps

# === === === MySQLテーブル関連 === === ===
def format_tables():
    sqls = (
        "DROP TABLE medicine.pac_insert;",
        """
        CREATE TABLE IF NOT EXISTS
        medicine.pac_insert(
            id BIGINT PRIMARY KEY AUTO_INCREMENT,
            companyid VARCHAR(6) NOT NULL,
            yjcode VARCHAR(12) NOT NULL,
            startdate TEXT,
            brandname TEXT NOT NULL,
            regulatory TEXT ,
            class TEXT,
            genericname TEXT,
            efficacy TEXT,
            doseadmin TEXT,
            contra TEXT,
            adverse TEXT,
            composition TEXT,
            uri VARCHAR(24) NOT NULL,
            created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            INDEX idx_code(yjcode)
        );
        """
    )
    for sql in sqls:
        cursor.execute(sql)
    connector.commit()

# === === === 実行プログラム === === ===
def sample_ubretid():
    UBRETID_TAB = '480306_1231014F1054_1_11.sgm'
    dic = sgm_to_dict(utf8_dir + UBRETID_TAB)
    for k, v in dic.items():
        print(f'{k}: {v}')

def output_csv_select_by_class():
    keyword = 'アレルギー性疾患'
    with open(keyword + '_2.csv', 'w') as f:
        sgm_file_name = os.listdir(utf8_dir)
        for name in tqdm(sgm_file_name):
            try:
                dics = sgm_to_dict_select_by_class(utf8_dir + name, keyword=keyword)
            except:
                continue

            if dics is None:
                continue

            for dic in dics:
                # dic -> list
                dic_list = [
                    dic['yjcode'], dic['brandname'], dic['class'], dic['regulatory'], dic['genericname'],
                    dic['efficacy'], dic['doseadmin'], dic['contra'], dic['adverse'],
                ]
                # データ整形 : None -> ''
                dic_list = [st if st else '' for st in dic_list]
                # データ整形 : list -> 'aaa,bbb,...'
                dic_list = [f'"{",".join(li)}"' if (type(li) is list) else li for li in dic_list]
                f.write(','.join(dic_list) + '\n')
                # for k, v in dic.items():
                #     print(f'{k}: {v}')

def insert_sgm_dict_into_table():
    format_tables()

    sgm_file_name = os.listdir(utf8_dir)
    for name in tqdm(sgm_file_name):
        dics = sgm_to_dict(utf8_dir + name)
        for dic in dics:
            # sql用にエスケープ処理
            for k, v in dic.items():
                if v:
                    dic[k] = v.replace('"', '""')
            # 要素の追加 > pac_insertテーブル
            sql = f""" 
            INSERT INTO medicine.pac_insert
            VALUES(
                NULL, 
                "{dic['companyid']}", 
                "{dic['yjcode']}", 
                "{dic['startdate']}", 
                "{dic['brandname']}", 
                "{dic['regulatory']}", 
                "{dic['class']}", 
                "{dic['genericname']}", 
                "{dic['efficacy']}", 
                "{dic['doseadmin']}", 
                "{dic['contra']}", 
                "{dic['adverse']}", 
                "{dic['composition']}", 
                "{name.replace('.sgm', '')}", 
                NULL             
            );
            """
            cursor.execute(sql)

    connector.commit()


if __name__ == '__main__':
    # sgm_file_name = os.listdir(utf8_dir)
    # cnt=0
    # for name in tqdm(sgm_file_name):
    #     cnt+=1
    #
    #     dic = sgm_to_dict(utf8_dir + name)
    #
    #     # if cnt % 1000 == 0:
    #     print('=' * 20)
    #     print(name)
    #     for k, v in dic.items():
    #         print(f'{k}: {v}')

    insert_sgm_dict_into_table()

    # cursor.close()
    # connector.close()
