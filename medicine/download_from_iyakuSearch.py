# coding: UTF-8

import random
import re
import time
from datetime import datetime
import zipfile
import codecs
import os
import glob

import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0'}
base_url = 'http://www.pmda.go.jp'
search_url = '/PmdaSearch/iyakuSearch/'
# sgml_url = '/PmdaSearch/iyakuDetail/ResultDataSetSGM/480235_3122007F2055_1_12'

root_dir = '/home/yama/iyaku/'
zip_dir = '/home/yama/iyaku/zip/'
sjis_dir = '/home/yama/iyaku/sjis/'
utf8_dir = '/home/yama/iyaku/utf8/'

file_drug_master = 'y_ALL20180828.csv'

WAIT_SEC = 0.5
TIMEOUT_SEC = 60

# global
i = 0
yj_code = ''
files_list = {}
f = open(root_dir + '00_downloaded_file.csv', 'a')

def download_doc_of_a_day(yyyymmdd):
    global files_list
    files_list = get_exists_files_list()

    linkDicts = getLinks(updateDocFrDt=yyyymmdd, updateDocToDt=yyyymmdd)
    if linkDicts is None:
        return None
    time.sleep(2)

    for linkDict in linkDicts:
        # 既存ファイルとの重複を確認
        if is_exists_sgml(linkDict['sgml']):
            time.sleep(WAIT_SEC)
            continue

        filename = getSGMLfile(linkDict['sgml'])
        if filename:
            datetime_ = str(datetime.now())[:-7]
            f.write(f'+,{datetime_},"{filename}","{linkDict["name"]}","{linkDict["comp"]}"\n')
        time.sleep(WAIT_SEC)

    f.close()

def show_updated_doc_list(yyyymmdd):
    global files_list
    files_list = get_exists_files_list()

    time.sleep(2)
    print(yyyymmdd)
    linkDicts = getLinks(updateDocFrDt=yyyymmdd, updateDocToDt=yyyymmdd)
    if linkDicts:
        cnt = 0
        for linkDict in linkDicts:
            if linkDict and 'sgml' in linkDict.keys():
                buf = is_exists_sgml(linkDict['sgml'], execute=False)
                # if buf is True or buf is False:
                if buf is True:
                    continue

                print('-'*10)
                print(buf)
                print(linkDict)
                cnt += 1
        print('-' * 10)
        print(cnt, '/', len(linkDicts))

    print('='*10)
    f.close()

def download_doc_from_csv_master():
    global i, yj_code, files_list
    files_list = get_exists_files_list()

    # 医薬品マスタ.csvからyj_codeを抽出: main_from_csv_master()
    codes = []
    with open(root_dir + file_drug_master, 'r') as f_master:
        for row in f_master:
            row = row.split(',')
            codes.append(row[31])

    for i, yj_code in tqdm(enumerate(codes), total=len(codes)):
        # if i < 16216:
        #     continue
        # print(i, yj_code)
        # exit()

        # 返り値の検証, 複数件あれば'*_?_00.sgm'の?が最小のものを使用
        linkDict = None
        linkDicts = getLinks(koumoku1Word=yj_code)
        if linkDicts:
            if len(linkDicts) == 1:
                linkDict = linkDicts[0]
            elif len(linkDicts) > 1:
                for k in range(1,9):
                    rows = [x for x in linkDicts
                            if get_file_version(x['sgml'])['subid'] == str(k)]
                    if len(rows) > 0:
                        linkDict = rows[0]
                        break

        if linkDict and 'sgml' in linkDict.keys():
            # 既存ファイルとの重複を確認
            if is_exists_sgml(linkDict['sgml']):
                time.sleep(WAIT_SEC)
                continue

            time.sleep(2)
            filename = getSGMLfile(linkDict['sgml'])
            if filename:
                datetime_ = str(datetime.now())[:-7]
                f.write(f'+,{datetime_},"{filename}","{linkDict["name"]}","{linkDict["comp"]}"\n')
        time.sleep(WAIT_SEC)

    f.close()

# PMDAのサイトでyj_codeを検索, 検索結果の情報を返す(100件まで)
def getLinks(**kwargs):
    if kwargs is None:
        return None

    # kwargsをpostするdataを代入
    data = {
        'nameWord': '',
        'iyakuHowtoNameSearchRadioValue': '1',
        'howtoMatchRadioValue': '1',
        'tglOpFlg': '',
        'dispColumnsList[0]': '1',
        'effectValue': '',
        'infoindicationsorefficacy': '',
        'infoindicationsorefficacyHowtoSearch': 'and',
        'warnings': '',
        'warningsHowtoSearch': 'and',
        'contraindicationsAvoidedadministration': '',
        'contraindicationsAvoidedadministrationHowtoSearch': 'and',
        'contraindicatedcombinationPrecautionsforcombination': '',
        'contraindicatedcombinationPrecautionsforcombinationHowtoSearch': 'and',
        'updateDocFrDt': '年月日[YYYYMMDD]',
        'updateDocToDt': '年月日[YYYYMMDD]',
        'compNameWord': '',
        'koumoku1Value': 'allsearch',
        'koumoku1Word': '',
        'koumoku1HowtoSearch': 'and',
        'koumoku2Value': '',
        'koumoku2Word': '',
        'koumoku2HowtoSearch': 'and',
        'koumoku3Value': '',
        'koumoku3Word': '',
        'koumoku3HowtoSearch': 'and',
        'howtoRdSearchSel': 'or',
        'relationDoc1Sel': '',
        'relationDoc1check1': 'on',
        'relationDoc1check2': 'on',
        'relationDoc1Word': '検索語を入力',
        'relationDoc1HowtoSearch': 'and',
        'relationDoc1FrDt': '年月[YYYYMM]',
        'relationDoc1ToDt': '年月[YYYYMM]',
        'relationDocHowtoSearchBetween12': 'and',
        'relationDoc2Sel': '',
        'relationDoc2check1': 'on',
        'relationDoc2check2': 'on',
        'relationDoc2Word': '検索語を入力',
        'relationDoc2HowtoSearch': 'and',
        'relationDoc2FrDt': '年月[YYYYMM]',
        'relationDoc2ToDt': '年月[YYYYMM]',
        'relationDocHowtoSearchBetween23': 'and',
        'relationDoc3Sel': '',
        'relationDoc3check1': 'on',
        'relationDoc3check2': 'on',
        'relationDoc3Word': '検索語を入力',
        'relationDoc3HowtoSearch': 'and',
        'relationDoc3FrDt': '年月[YYYYMM]',
        'relationDoc3ToDt': '年月[YYYYMM]',
        'ListRows': '100',
        # 'btnA.x': '69',
        # 'btnA.y': '14',
        'btnA': '検索',
        'listCategory': ''
    }
    for k, v in kwargs.items():
        if k in data.keys():
            data[k] = str(v)

    link_dicts = []
    try:
        html = requests.post(base_url + search_url, data=data, timeout=TIMEOUT_SEC, headers=headers)
    except:
        time.sleep(WAIT_SEC)
        html = requests.post(base_url + search_url, data=data, timeout=TIMEOUT_SEC, headers=headers)
    soup = BeautifulSoup(html.text, 'html.parser')

    # 検索結果の件数, ここで0件を排除.
    nRes = soup.find('strong', text=re.compile('^検索結果([0-9]+)件.*$'))
    if nRes is None:
        return None
    # nRes = re.match('^検索結果([0-9]+)件.*$', nRes.get_text()).group(1)

    # 検索結果の表をlistに整形
    rows = soup.find('table', {'id': 'ResultList'}).findAll('tr')
    for row in rows[1:]:    # 先頭行はヘッダー
        links = row.findAll('td')
        link_dict = {
            'name': links[1].get_text(),
            'comp': links[2].get_text('|', strip=True)
        }

        sgml = links[3].find('a', text="SGML")
        if sgml:
            link_dict['sgml'] = sgml.attrs['href']
        link_dicts.append(link_dict)

    return link_dicts

# urlからzipファイルをダウンロード -> 解凍 -> .sgmファイルのみ抽出
def getSGMLfile(url):
    # zipダウンロード
    filename = url.split('/')[-1]
    r = requests.get(base_url+url, stream=True)
    with open(zip_dir+filename, 'wb') as zf:
        zf.write(r.content)

    # zip解凍
    with zipfile.ZipFile(zip_dir+filename, 'r') as zf:
        members = [n for n in zf.namelist() if n.endswith('.sgm')]
        zf.extractall(sjis_dir, members)

    # 文字コード変換(shift-jis > utf-8)
    filename = members[0]
    fin = codecs.open(sjis_dir+filename, 'r', 'shift_jis')
    fout = codecs.open(utf8_dir+filename, 'w', 'utf-8')
    for row in fin:
        if 'ENCODING shift-jis' in row:
            row = row.replace('shift-jis', 'utf-8')
        fout.write(row)
    fin.close()
    fout.close()
    os.remove(sjis_dir+filename)

    # files_listを更新
    global files_list
    files_list[get_file_version(filename)['code']] = filename

    return filename

def get_file_version(filename:str):
    filename = filename.split('/')[-1]
    lis = filename.replace('.', '_').split('_')
    info = {
        'code': lis[1],
        'subid': lis[2],
        'subver': lis[2]
    }
    return info

# 同一yjcodeの添付文書ファイルが存在するか確認 -> ログファイルに出力
def is_exists_sgml(sgml, execute=True):
    global files_list
    filename = sgml.split('/')[-1] + '.sgm'
    code = filename.split('_')[1]
    if code in files_list.keys():
        info_new = get_file_version(filename)
        info_old = get_file_version(files_list[code])

        # ファイルの追加、更新はしない
        if info_new['subid'] != info_old['subid']:
            return True
        if info_new['subver'] <= info_old['subver']:
            return True

        # ファイルの更新: 旧ファイルを削除
        if execute:
            os.remove(utf8_dir+filename)
            datetime_ = str(datetime.now())[:-7]
            f.write(f'-,{datetime_},"{filename}",,\n')
        else:
            # print(filename, '<-',files_list[code])
            return ' '.join((filename, '<-', files_list[code]))
    return False

# utf8_dirの*.sgm一覧を取得 -> dict{code: filename}
def get_exists_files_list():
    files_list = {}
    rows = glob.glob(utf8_dir + '*.sgm')
    for row in rows:
        filename = row.split('/')[-1]
        code = filename.split('_')[1]
        files_list[code] = filename
    return files_list

if __name__ == '__main__':
    dict_ = {
        'yj_code': '2325003B2029',
        'name': 'ガスター散2％／ガスター散10％',
        'maker': '製造販売／アステラス製薬株式会社',
        'sgml': '/PmdaSearch/iyakuDetail/ResultDataSetSGM/800126_2325003B1022_1_13'
    }

    # download_doc_from_csv_master()
    # exit()

    # for i in range(1,12):
    #     yyyymmdd = f'201809{i:0>2}'
    #     show_updated_doc_list(yyyymmdd)
    # # show_updated_doc_list(20180208)
    # exit()


    from Wikipedia.mailer import send_nortification
    print(str(datetime.now())[:-7])
    try:
        download_doc_from_csv_master()
        print('\nProcess finished')
        send_nortification('iyaku - Process finished', '')
    except Exception as e:
        msg = f'#{i}: {yj_code}\n\n'
        msg += str(e.args[0])
        print('\n', msg)
        send_nortification('iyaku - Exception notice', msg)
    print(str(datetime.now())[:-7])

