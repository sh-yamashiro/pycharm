# coding: UTF-8
"""
@citation: https://qiita.com/Azunyan1111/items/9b3d16428d2bcc7c9406
@citation: http://vaaaaaanquish.hatenablog.com/entry/2017/06/25/202924
"""
import mysql.connector
from prettytable import PrettyTable, from_db_cursor

root_dir = '/home/yama/iyaku/'

connector = mysql.connector.connect(
    host="localhost", user="root", passwd="root", db="medicine", charset="utf8mb4")
cursor = connector.cursor(buffered=True)

# === データベース検索 ===
def search_generic():
    row = search_and_select_drug(key='メトホルミン', idx=3)

    # 選択した規格の薬品一覧を表示
    header = ['yjcode', 'price', 'name', 'maker', 'is_s', 'has_g', 'is_g']
    sql = f"""
    SELECT {','.join(header)} 
    FROM brand WHERE component = '{row[0]}' AND size = '{row[1]}';
    """
    cursor.execute(sql)
    rows = cursor.fetchall()
    new_rows = []
    new_header = ['yjcode', '', 'price', 'name', 'maker']
    # 先発後発の分類
    for row in rows:
        row = (row[0], sg_3bool_to_symbol(row[4:7])) + row[1:4]
        new_rows.append(row)

    output_sql(new_rows, new_header)
    print('[*]先発（後発なし） [o]先発（後発あり） [ ]後発ほか')

def search_generic_compound():
    row = search_and_select_drug(key='メトホルミン')

    # 選択した規格の薬品一覧を表示
    header = ['yjcode', 'price', 'name', 'maker', 'is_s', 'has_g', 'is_g']
    sql = f"""
    SELECT {','.join([f'brand.{k}' for k in header])}, pac_insert.composition
    FROM brand JOIN pac_insert ON brand.yjcode = pac_insert.yjcode
    WHERE brand.component = '{row[0]}' AND brand.size = '{row[1]}';
    """
    cursor.execute(sql)
    rows = cursor.fetchall()
    new_rows = []
    new_header = ['yjcode', '', 'price', 'name', 'maker', 'composition']
    # 先発後発の分類
    for row in rows:
        row = (row[0], sg_3bool_to_symbol(row[4:7])) + row[1:4] + (row[7],)
        new_rows.append(row)

    output_sql(new_rows, new_header)
    print('[*]先発（後発なし） [o]先発（後発あり） [ ]後発ほか')

def search_equieffective(exclude_g:bool=True):
    # key = input('成分名, または商品名を入力 >> ')
    key = 'アムロジン'
    key = 'ジャヌビア'
    code = sql_select_yjcode_where_name_is_(key)[:4]

    # 薬品一覧を表示
    print(code)
    header = ['yjcode', 'price', 'name', 'maker', 'is_s', 'has_g', 'is_g']
    sql = f"""
    SELECT {','.join(header)} FROM brand 
    WHERE class = '内用薬' {"AND is_g = 0" if exclude_g else ""} AND yjcode LIKE '{code}%';
    """
    cursor.execute(sql)
    rows = cursor.fetchall()
    new_rows = []
    new_header = ['yjcode', '', 'price', 'name', 'maker']
    # 先発後発の分類
    for row in rows:
        row = (row[0], sg_3bool_to_symbol(row[4:7])) + row[1:4]
        new_rows.append(row)

    output_sql(new_rows, new_header)
    print('[*]先発（後発なし） [o]先発（後発あり） [ ]後発ほか')

def search_combination():
    # key = input('成分名, または商品名を入力 >> ')
    key = 'メトホルミン'
    #
    # 一覧を表示
    sql = f"""
    SELECT component, count(*) 
    FROM brand WHERE component LIKE '%{key}%' OR name LIKE '%{key}%' 
    GROUP BY component;
    """
    cursor.execute(sql)
    rows = cursor.fetchall()
    for i, row in enumerate(rows):
        print(f'{i:>2}: {row[0]} ({row[1]})')

    # 成分(組み合わせ)を選択
    try:
        idx = int(input('成分を選択 >> '))
    except:
        idx = 0
    if idx < 0 or idx >= len(rows):
        exit()
    row = rows[idx]
    print(f'\n{row[0]}')
    ingreds = row[0].split("・")

    # 選択した成分の薬品一覧を表示
    header = ['yjcode', 'price', 'name', 'maker', 'is_s', 'has_g', 'is_g']
    sql = f"""
    SELECT {','.join(header)} 
    FROM brand WHERE component = '{row[0]}';
    """
    cursor.execute(sql)
    rows = cursor.fetchall()
    new_rows = []
    new_header = ['yjcode', '', 'price', 'name', 'maker']
    # 先発後発の分類
    for row in rows:
        row = (row[0], sg_3bool_to_symbol(row[4:7])) + row[1:4]
        new_rows.append(row)

    # 単剤の情報を取得
    if len(ingreds) >= 2:
        for idx, ingred in enumerate(ingreds):
            sql = f"""
            SELECT {','.join(header[0:4])} 
            FROM brand WHERE component = '{ingred}' AND (is_s = b'1' OR has_g = b'1');
            """
            cursor.execute(sql)
            rows = cursor.fetchall()
            for row in rows:
                row = (row[0], idx+1) + row[1:]
                new_rows.append(row)
    # 結果の出力
    output_sql(new_rows, new_header)
    print('[*]先発（後発なし） [o]先発（後発あり） [ ]後発ほか [1...]単剤（配合剤検索時のみ）')

# === 検索用メソッド ===
# key: 成分名,一般名 -> row: [component, size]
def search_and_select_drug(key:str = '', idx=None):
    if key == '':
        key = input('成分名, または商品名を入力 >> ')

    # 規格一覧を表示
    sql = f"""
    SELECT component, size, count(*) 
    FROM brand WHERE component LIKE '%{key}%' OR name LIKE '{key}%' 
    GROUP BY component, size;
    """
    cursor.execute(sql)
    rows = cursor.fetchall()
    for i, row in enumerate(rows):
        print(f'{i:>2}: {row[0]} {row[1]} ({row[2]})')

    # 規格を選択
    if idx is None:
        try:
            idx = int(input('規格を選択 >> '))
        except:
            idx = 0
    if idx < 0 or idx >= len(rows):
        exit()
    row = rows[idx]
    print(f'\n{row[0]} {row[1]}')

    return row

# "name"keyの先頭一致でyjcodeを検索
def sql_select_yjcode_where_name_is_(key:str):
    sql = f"""
    SELECT yjcode FROM brand WHERE name LIKE '{key}%';
    """
    cursor.execute(sql)
    rows = cursor.fetchone()
    code = rows[0]
    # rows = cursor.fetchall()
    # rows = set(c[0][:4] for c in rows)
    return code

# === 表示用メソッド ===
# row: [is_s, has_g, is_g] -> symbol: {Sのみ: '*', S(Gあり): 'o', G: ' '}
def sg_3bool_to_symbol(row:list):
    if row[0]:
        return '*'
    elif row[1]:
        return 'o'
    else:
        return ' '

def output_sql(rows:list, header:list, show_vertical:bool=False):
    # MySQLの垂直表示スタイル(\G)
    if show_vertical:
        for i, row in enumerate(rows):
            print('*' * 20 + f' {i+1}. row ' + '*' * 20)
            for h, r in zip(header, row):
                print(f'{h[:14]:>14}: {r}')
        return

    # headerをもつテーブルを作る
    table = PrettyTable(header)
    # 列を左詰めする
    table._set_align('l')
    table.align['price'] = 'r'
    # 左右の空白を設定
    table.padding_width = 2
    # データを入れる
    for row in rows:
        table.add_row(row)
    # テーブルを標準出力に書き出す
    print(table)

if __name__ == '__main__':
    # search_generic()
    # search_combination()
    # search_equieffective()
    search_generic_compound()

    cursor.close()
    connector.close()

